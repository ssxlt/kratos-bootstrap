package registry

import (
	"github.com/go-kratos/kratos/v2/registry"

	"gitee.com/ssxlt/kratos-bootstrap/registry/consul"
	"gitee.com/ssxlt/kratos-bootstrap/registry/etcd"
	"gitee.com/ssxlt/kratos-bootstrap/registry/eureka"
	"gitee.com/ssxlt/kratos-bootstrap/registry/kubernetes"
	"gitee.com/ssxlt/kratos-bootstrap/registry/nacos"
	"gitee.com/ssxlt/kratos-bootstrap/registry/servicecomb"
	"gitee.com/ssxlt/kratos-bootstrap/registry/zookeeper"

	conf "gitee.com/ssxlt/kratos-bootstrap/api/gen/go/conf/v1"
)

// NewRegistry 创建一个注册客户端
func NewRegistry(cfg *conf.Registry) registry.Registrar {
	if cfg == nil {
		return nil
	}

	switch Type(cfg.Type) {
	case Consul:
		return consul.NewRegistry(cfg)
	case Etcd:
		return etcd.NewRegistry(cfg)
	case ZooKeeper:
		return zookeeper.NewRegistry(cfg)
	case Nacos:
		return nacos.NewRegistry(cfg)
	case Kubernetes:
		return kubernetes.NewRegistry(cfg)
	case Eureka:
		return eureka.NewRegistry(cfg)
	case Polaris:
		return nil
	case Servicecomb:
		return servicecomb.NewRegistry(cfg)
	}

	return nil
}

// NewDiscovery 创建一个发现客户端
func NewDiscovery(cfg *conf.Registry) registry.Discovery {
	if cfg == nil {
		return nil
	}

	switch Type(cfg.Type) {
	case Consul:
		return consul.NewRegistry(cfg)
	case Etcd:
		return etcd.NewRegistry(cfg)
	case ZooKeeper:
		return zookeeper.NewRegistry(cfg)
	case Nacos:
		return nacos.NewRegistry(cfg)
	case Kubernetes:
		return kubernetes.NewRegistry(cfg)
	case Eureka:
		return eureka.NewRegistry(cfg)
	case Polaris:
		return nil
	case Servicecomb:
		return servicecomb.NewRegistry(cfg)
	}

	return nil
}
