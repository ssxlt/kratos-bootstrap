#!/bin/bash

set -x
#指定起始文件夹
DIR=`pwd`

cd $DIR/config/apollo
go get all
go mod tidy

cd $DIR/config/consul
go get all
go mod tidy

cd $DIR/config/etcd
go get all
go mod tidy

cd $DIR/config/kubernetes
go get all
go mod tidy

cd $DIR/config/nacos
go get all
go mod tidy

cd $DIR/config/polaris
go get all
go mod tidy

cd $DIR/logger/aliyun
go get all
go mod tidy

cd $DIR/logger/fluent
go get all
go mod tidy

cd $DIR/logger/logrus
go get all
go mod tidy

cd $DIR/logger/tencent
go get all
go mod tidy

cd $DIR/logger/zap
go get all
go mod tidy

cd $DIR/oss/minio
go get all
go mod tidy

cd $DIR/registry/consul
go get all
go mod tidy

cd $DIR/registry/etcd
go get all
go mod tidy

cd $DIR/registry/eureka
go get all
go mod tidy

cd $DIR/registry/kubernetes
go get all
go mod tidy

cd $DIR/registry/nacos
go get all
go mod tidy

cd $DIR/registry/polaris
go get all
go mod tidy

cd $DIR/registry/servicecomb
go get all
go mod tidy

cd $DIR/registry/zookeeper
go get all
go mod tidy

cd $DIR
go get gitee.com/ssxlt/kratos-bootstrap/logger/logrus@v1.0.2
go get gitee.com/ssxlt/kratos-bootstrap/logger/zap@v1.0.8
go get gitee.com/ssxlt/kratos-bootstrap/logger/tencent@v1.0.2
go get gitee.com/ssxlt/kratos-bootstrap/logger/fluent@v1.0.2
go get gitee.com/ssxlt/kratos-bootstrap/api@v1.0.4
go get gitee.com/ssxlt/kratos-bootstrap/logger/aliyun@v1.0.1
go get gitee.com/ssxlt/kratos-bootstrap/cache/redis@v1.0.1
go get gitee.com/ssxlt/kratos-bootstrap/oss/minio@v1.0.1
go get gitee.com/ssxlt/kratos-bootstrap/config/apollo@v1.0.1
go get gitee.com/ssxlt/kratos-bootstrap/config/consul@v1.0.1
go get gitee.com/ssxlt/kratos-bootstrap/config/etcd@v1.0.1
go get gitee.com/ssxlt/kratos-bootstrap/config/kubernetes@v1.0.1
go get gitee.com/ssxlt/kratos-bootstrap/config/nacos@v1.0.1
go get gitee.com/ssxlt/kratos-bootstrap/config/polaris@v1.0.1
go get gitee.com/ssxlt/kratos-bootstrap/registry/consul@v1.0.1
go get gitee.com/ssxlt/kratos-bootstrap/registry/etcd@v1.0.1
go get gitee.com/ssxlt/kratos-bootstrap/registry/eureka@v1.0.1
go get gitee.com/ssxlt/kratos-bootstrap/registry/kubernetes@v1.0.1
go get gitee.com/ssxlt/kratos-bootstrap/registry/nacos@v1.0.1
go get gitee.com/ssxlt/kratos-bootstrap/registry/polaris@v1.0.1
go get gitee.com/ssxlt/kratos-bootstrap/registry/servicecomb@v1.0.1
go get gitee.com/ssxlt/kratos-bootstrap/registry/zookeeper@v1.0.1
go get all
go mod tidy