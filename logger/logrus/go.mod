module gitee.com/ssxlt/kratos-bootstrap/logger/logrus

go 1.22.3

require (
	gitee.com/ssxlt/kratos-bootstrap/api v1.0.4
	github.com/go-kratos/kratos/contrib/log/logrus/v2 v2.0.0-20240606021638-ba5a651b8252
	github.com/go-kratos/kratos/v2 v2.7.3
	github.com/sirupsen/logrus v1.9.3
)

require (
	golang.org/x/sys v0.21.0 // indirect
	google.golang.org/protobuf v1.34.1 // indirect
)
