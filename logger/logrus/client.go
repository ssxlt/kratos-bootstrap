package logrus

import (
	"fmt"

	conf "gitee.com/ssxlt/kratos-bootstrap/api/gen/go/conf/v1"
	logrusLogger "github.com/go-kratos/kratos/contrib/log/logrus/v2"
	"github.com/go-kratos/kratos/v2/log"
	"github.com/sirupsen/logrus"
)

// NewLogger 创建一个新的 Logrus 日志记录器
func NewLogger(cfg *conf.Logger) log.Logger {
	// 解析日志级别
	loggerLevel, err := logrus.ParseLevel(cfg.Logrus.Level)
	if err != nil {
		// 打印警告，但使用默认日志级别
		fmt.Printf("Failed to parse log level: %s, using default level Info\n", cfg.Logrus.Level)
		loggerLevel = logrus.InfoLevel
	}

	// 设置日志格式化器
	var loggerFormatter logrus.Formatter
	switch cfg.Logrus.Formatter {
	case "json":
		loggerFormatter = &logrus.JSONFormatter{
			DisableTimestamp: cfg.Logrus.DisableTimestamp,
			TimestampFormat:  cfg.Logrus.TimestampFormat,
		}
	default: // 包括 "text" 和其他任何未识别的值
		loggerFormatter = &logrus.TextFormatter{
			DisableColors:    cfg.Logrus.DisableColors,
			DisableTimestamp: cfg.Logrus.DisableTimestamp,
			TimestampFormat:  cfg.Logrus.TimestampFormat,
		}
	}

	// 创建和配置 Logrus 日志记录器
	logger := logrus.New()
	logger.Level = loggerLevel
	logger.Formatter = loggerFormatter

	// 创建 Kratos 日志记录器包装器
	wrapped := logrusLogger.NewLogger(logger)

	return wrapped
}
