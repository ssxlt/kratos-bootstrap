package zap

import (
	"log/slog"
	"os"

	zapLogger "github.com/go-kratos/kratos/contrib/log/zap/v2"

	"github.com/go-kratos/kratos/v2/log"

	conf "gitee.com/ssxlt/kratos-bootstrap/api/gen/go/conf/v1"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"gopkg.in/natefinch/lumberjack.v2"
)

// NewLogger 创建一个新的日志记录器 - Zap
func NewLogger(cfg *conf.Logger) log.Logger {
	encoderConfig := zap.NewProductionEncoderConfig()
	encoderConfig.EncodeTime = zapcore.ISO8601TimeEncoder
	encoderConfig.TimeKey = "time"
	encoderConfig.EncodeLevel = zapcore.CapitalLevelEncoder
	encoderConfig.EncodeDuration = zapcore.SecondsDurationEncoder
	encoderConfig.EncodeCaller = zapcore.ShortCallerEncoder
	jsonEncoder := zapcore.NewJSONEncoder(encoderConfig)

	var lvl zapcore.Level
	if err := lvl.UnmarshalText([]byte(cfg.Zap.Level)); err != nil {
		lvl = zapcore.InfoLevel
		slog.Warn("Can not UnmarshalText cfg.Zap.Level, err: ", err)
	}

	var logWriter zapcore.WriteSyncer
	var multiWriter zapcore.WriteSyncer
	if cfg.Zap.EnableFile {
		lumberJackLogger := &lumberjack.Logger{
			Filename:   cfg.Zap.Filename,
			MaxSize:    int(cfg.Zap.MaxSize),
			MaxBackups: int(cfg.Zap.MaxBackups),
			MaxAge:     int(cfg.Zap.MaxAge),
			Compress:   cfg.Zap.Compress,
		}
		logWriter = zapcore.AddSync(lumberJackLogger)
		multiWriter = zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout), logWriter)
	} else {
		multiWriter = zapcore.NewMultiWriteSyncer(zapcore.AddSync(os.Stdout))
	}

	core := zapcore.NewCore(jsonEncoder, multiWriter, lvl)

	logger := zap.New(core).WithOptions()

	wrapped := zapLogger.NewLogger(logger)

	return wrapped
}
