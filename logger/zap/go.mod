module gitee.com/ssxlt/kratos-bootstrap/logger/zap

go 1.22.3

require (
	gitee.com/ssxlt/kratos-bootstrap/api v1.0.4
	github.com/go-kratos/kratos/contrib/log/zap/v2 v2.0.0-20240606021638-ba5a651b8252
	github.com/go-kratos/kratos/v2 v2.7.3
	go.uber.org/zap v1.27.0
	gopkg.in/natefinch/lumberjack.v2 v2.2.1
)

require (
	go.uber.org/multierr v1.11.0 // indirect
	google.golang.org/protobuf v1.34.1 // indirect
)
