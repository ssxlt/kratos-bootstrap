module gitee.com/ssxlt/kratos-bootstrap/logger/fluent

go 1.22.3

require (
	gitee.com/ssxlt/kratos-bootstrap/api v1.0.4
	github.com/go-kratos/kratos/contrib/log/fluent/v2 v2.0.0-20240606021638-ba5a651b8252
	github.com/go-kratos/kratos/v2 v2.7.3
)

require (
	github.com/fluent/fluent-logger-golang v1.9.0 // indirect
	github.com/philhofer/fwd v1.1.2 // indirect
	github.com/tinylib/msgp v1.1.9 // indirect
	google.golang.org/protobuf v1.34.1 // indirect
)
