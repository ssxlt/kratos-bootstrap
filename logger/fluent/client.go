package fluent

import (
	conf "gitee.com/ssxlt/kratos-bootstrap/api/gen/go/conf/v1"
	fluentLogger "github.com/go-kratos/kratos/contrib/log/fluent/v2"
	"github.com/go-kratos/kratos/v2/log"
)

// NewLogger 创建一个新的日志记录器 - Fluent
func NewLogger(cfg *conf.Logger) log.Logger {
	wrapped, err := fluentLogger.NewLogger(cfg.Fluent.Endpoint)
	if err != nil {
		// 发生错误时直接panic
		panic("create fluent logger failed: " + err.Error())
	}
	// 如果没有错误，返回创建的日志记录器
	return wrapped
}
