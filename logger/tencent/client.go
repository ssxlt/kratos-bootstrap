package tencent

import (
	"fmt"

	conf "gitee.com/ssxlt/kratos-bootstrap/api/gen/go/conf/v1"
	tencentLogger "github.com/go-kratos/kratos/contrib/log/tencent/v2"
	"github.com/go-kratos/kratos/v2/log"
)

// NewLogger 创建一个新的腾讯云日志记录器
func NewLogger(cfg *conf.Logger) log.Logger {
	// 使用腾讯云日志服务的配置创建日志记录器
	wrapped, err := tencentLogger.NewLogger(
		tencentLogger.WithTopicID(cfg.Tencent.TopicId),
		tencentLogger.WithEndpoint(cfg.Tencent.Endpoint),
		tencentLogger.WithAccessKey(cfg.Tencent.AccessKey),
		tencentLogger.WithAccessSecret(cfg.Tencent.AccessSecret),
	)
	if err != nil {
		// 打印警告
		fmt.Printf("Failed to parse log config: %s\n", err)
		return nil
	}
	// 成功创建日志记录器后返回
	return wrapped
}
